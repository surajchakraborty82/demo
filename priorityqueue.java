package queue;

import java.util.PriorityQueue;

public class priorityqueue {

	public static void main(String[] args) {

		System.out.println("---------------Welcome to Inox cinemas--------------");
		System.out.println("Please stand in a Queue\n");

		// Creating an empty LinkedList of String type
		PriorityQueue<String> queue = new PriorityQueue<String>();

		// Adding elements in the list Using add() method
		queue.add("Amit Sharma");
		queue.add("Mukesh Yadav");
		queue.add("Kiran Agarwal");
		queue.add("Lavanya Mehta");
		queue.add("Umesh Pandit");

		// Printing the elements inside ArrayList
		System.out.println("Booking Row:" + queue);
		System.out.println("First Head:" + queue.peek());
		System.out.println("\n--------------Start Giving Movie Tickets--------------\n");

		String s = queue.poll();
		String k = queue.poll();
		// Printing the final elements inside ArrrayList
		System.out.println("Tickets sold to: " + s);
		System.out.println("Tickets sold to: " + k);
		System.out.println("Remaining Queue:" + queue);
		System.out.println("\n----------------------Thank You For Choosing Us.----------------------");
	}

}
