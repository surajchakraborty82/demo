package queue;

import java.util.ArrayDeque;
import java.util.Deque;

public class deque {

	public static void main(String[] args) {
		System.out.println("---------------Welcome to Inox cinemas--------------");
		System.out.println("Please stand in a Queue\n");

		// Creating an empty dequeue of String type
		Deque<String> deque = new ArrayDeque<String>();

		// Adding elements in the list Using add() method
		deque.add("Amit Sharma");
		deque.add("Mukesh Yadav");
		deque.add("Kiran Agarwal");
		deque.add("Lavanya Mehta");
		deque.add("Umesh Pandit");

		// Printing the elements inside dequeue
		System.out.println("Booking Row:" + deque);
		System.out.println("First Head:" + deque.peek());
		System.out.println("\n--------------Start Giving Movie Tickets--------------\n");

		String s = deque.poll();
		String k = deque.poll();

		// Printing the final elements inside dequeue
		System.out.println("Tickets sold to: " + s);
		System.out.println("Tickets sold to: " + k);
		System.out.println("Remaining Queue:" + deque);
		System.out.println("\n----------------------Thank You For Choosing Us.----------------------");
	}

}
