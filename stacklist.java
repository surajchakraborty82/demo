package lists;

import java.util.Stack;

public class stacklist {
	public static void main(String[] args) {

		System.out.println("Welcome to Amazon Cart\n");
		System.out.println("Please select your items to buy\n");

		// Creating an empty LinkedList of String type
		Stack<String> stack = new Stack<String>();
		stack.push("Redmi Note 10");
		stack.push("One plus Nord");
		stack.push("Iphone 13");
		stack.push("Samsung M30");
		stack.push("Poco M3");

		// Printing the elements inside ArrayList
		System.out.println("My cart:" + stack);
		System.out.println("\nconfirm your choices\n");

		// Removing the head from List using pop() method
		String k = stack.pop();
		String s = stack.pop();

		// Printing the final elements inside ArrrayList
		System.out.println("Removed item: " + k + s);
		System.out.println("Final cart:" + stack);
		System.out.println("\nThank You For Shpping With Us.");
	}

}
