package sets;

import java.util.*;

//Main class
//HashSet demo
public class hashset {
	public static void main(String args[]) {
		System.out.println("Welcome to Amazon Cart\n");
		System.out.println("Please select your items to buy\n");
		// Creating an empty LinkedHashSet
		LinkedHashSet<String> set = new LinkedHashSet<String>();

		// Use add() method to add elements into the Set
		set.add("Redmi Note 10");
		set.add("One plus Nord");
		set.add("Samsung M30");
		set.add("Iphone 13");
		set.add("Samsung M30");
		set.add("Poco M3");

		// Displaying the LinkedHashSet
		System.out.println("My Cart: " + set);
		System.out.println("\nconfirm your choices\n");

		// Removing elements using remove() method
		set.remove("Iphone 13");
		set.remove("Poco M3");
		set.remove("Redmi Note 10");

		// Displaying the LinkedHashSet after removal
		System.out.println("Final Cart: " + set);
		System.out.println("\nThank You For Shpping With Us.");
	}
}