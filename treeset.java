package sets;

import java.util.TreeSet;

public class treeset {
	public static void main(String args[]) {
		System.out.println("Welcome to Amazon Cart\n");
		System.out.println("Please select your items to buy\n");

		// Creating an empty TreeSet
		TreeSet<String> tree = new TreeSet<String>();

		// Use add() method to add elements into the Set
		tree.add("Redmi Note 10");
		tree.add("One plus Nord");
		tree.add("Iphone 13");
		tree.add("Samsung M30");
		tree.add("Poco M3");
		tree.add("Samsung M30");

		// Displaying the TreeSet
		System.out.println("My Cart: " + tree);
		System.out.println("\nconfirm your choices\n");

		// Removing elements using remove() method
		tree.remove("Iphone 13");
		tree.remove("Redmi Note 10");
		tree.remove("Poco M3");

		// Displaying the TreeSet after removal
		System.out.println("Final Cart: " + tree);
		System.out.println("\nThank You For Shpping With Us.");
	}
}