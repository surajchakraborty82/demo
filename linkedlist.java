package lists;

import java.util.LinkedList;

// Main class
public class linkedlist {

	// Main driver method
	public static void main(String args[]) {

		System.out.println("Welcome to Amazon Cart\n");
		System.out.println("Please select your items to buy\n");

		// Creating an empty LinkedList of String type
		LinkedList<String> list = new LinkedList<String>();

		// Adding elements in the list Using add() method
		list.add("Redmi Note 10");
		list.add("One plus Nord");
		list.add("Iphone 13");
		list.add("Samsung M30");
		list.add("Poco M3");

		// Printing the elements inside LinkedList
		System.out.println("My cart:" + list);
		System.out.println("\nconfirm your choices\n");

		// Removing the head from List using remove() method
		String k = list.remove(4);

		// Printing the final elements inside LinkedList
		System.out.println("Removed item: " + k);
		System.out.println("Final cart:" + list);
		System.out.println("\nThank You For Shpping With Us.");
	}
}