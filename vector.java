package lists;

import java.util.*;

public class vector {
	public static void main(String args[]) {
		System.out.println("Welcome to Amazon Cart\n");
		System.out.println("Please select your items to buy\n");
		// Creating an empty Vector
		Vector<String> vec_tor = new Vector<String>();

		// Use add() method to add elements in the Vector
		vec_tor.add("Redmi Note 10");
		vec_tor.add("One plus Nord");
		vec_tor.add("Iphone 13");
		vec_tor.add("Samsung M30");
		vec_tor.add("Poco M3");

		// Output the Vector
		System.out.println("My Cart: " + vec_tor);
		System.out.println("\nconfirm your choices\n");
		// Remove the element using remove()
		String k = vec_tor.remove(4);
		String s = vec_tor.remove(0);

		// Print the removed element
		System.out.println("Removed item: " + k);
		System.out.println("Removed item: " + s);

		// Print the final Vector
		System.out.println("Final Cart: " + vec_tor);
		System.out.println("\nThank You For Shpping With Us.");
	}
}
